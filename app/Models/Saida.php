<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Saida extends Model {

	public $timestamps = true;
	protected $primaryKey = 'id';
	protected $table = 'saidas';
	protected $fillable = ['data', 'valor', 'valor_individual', 'descricao', 'forma_pagamento_id', 'conta_dividida', 'conta_dividida_igualmente'];

	protected $hidden = [
        'child_id', 'forma_pagamento_id',
    ];

	public function scopeWithAll($query) {
        return $query->with(['forma_pagamento', 'tags', 'user', 'childs']);
        // return $query->with(['forma_pagamento', 'tags', 'user']);
    }

	public function forma_pagamento() {
		return $this->hasOne('App\Models\FormaPagamento', 'id', 'forma_pagamento_id');
	}

	public function tags() {
		return $this->belongsToMany('App\Models\Tag', 'many_saidas_tags', 'saida_id', 'tag_id');
	}

	public function user() {
		return $this->hasOne('App\Models\Auth\User\User', 'id', 'user_id');
	}

	public function childs() {
		return $this->hasMany('App\Models\Saida', 'child_id', 'id')->with('user');
	}

}
