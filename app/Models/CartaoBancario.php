<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartaoBancario extends Model {

  public $timestamps = false;
  protected $primaryKey = 'id';
  protected $table = 'cartao_bancario';  
    // protected $hidden = ['CHAVEALTOQI'];

  public function banco() {
    return $this->belongsTo('App\Models\Banco', 'banco_id', 'id');
  }

  public function bandeira() {
    return $this->belongsTo('App\Models\BandeiraCartao', 'bandeira_cartao_id', 'id');
  }

}
