<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ValeAlimentacaoRefeicao extends Model {

	public $timestamps = false;
	protected $primaryKey = 'id';
	protected $table = 'ticket_comida';

}
