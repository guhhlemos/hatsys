<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormaPagamento extends Model {

	public $timestamps = false;
	protected $primaryKey = 'id';
	protected $table = 'forma_pagamento';

	public function scopeWithAll($query) {
        return $query->with(['cartao_bancario', 'vale_alimentacao_refeicao']);
    }

	public function cartao_bancario() {
		return $this->hasOne('App\Models\CartaoBancario', 'id', 'cartao_bancario_id');
	}

	public function vale_alimentacao_refeicao() {
		return $this->hasOne('App\Models\ValeAlimentacaoRefeicao', 'id', 'vale_alimentacao_refeicao_id');
	}

}
