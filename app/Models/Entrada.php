<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entrada extends Model {

	public $timestamps = false;
	protected $primaryKey = 'id';
	protected $table = 'entradas';
	protected $fillable = ['data', 'valor', 'descricao'];

	public function scopeWithAll($query) {
        return $query->with(['tags']);
    }

	public function tags() {
		return $this->belongsToMany('App\Models\Tag', 'many_entradas_tags', 'entrada_id', 'tag_id');
	}

}
