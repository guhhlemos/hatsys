<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

	public $timestamps = false;
	protected $primaryKey = 'id';
	protected $table = 'tags';  

	// public function cartao() {
	// 	return $this->hasMany('App\Models\CartaoBancario', 'banco_id', 'id');
	// }

}
