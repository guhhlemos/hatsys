<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model {

	public $timestamps = false;
	protected $primaryKey = 'id';
	protected $table = 'banco';  

	public function cartao() {
		return $this->hasMany('App\Models\CartaoBancario', 'banco_id', 'id');
	}

}
