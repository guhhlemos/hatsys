<?php

namespace App\Http\Controllers\Financeiro;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\User;
use App\Models\Banco;
use App\Models\CartaoBancario;
use App\Models\BandeiraCartao;
use App\Models\Saida;
use App\Models\Entrada;
use App\Models\FormaPagamento;
use App\Models\Tag;

class EntradaController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index() {

        return view('financeiro.entrada.index')->with([
            'entradas' => Entrada::withAll()
            ->orderBy('data', 'desc')
            ->orderBy('id', 'desc')
            ->where('user_id', Auth::id())
            ->whereMonth('data', '=', date('m'))
            ->get(),
        ]);
    }

    public function create() {

        return view('financeiro.entrada.cadastrar')->with([
            'tags' => Tag::all(),
        ]);
    }

    public function edit($id) {

        return view('financeiro.entrada.cadastrar')->with([
            'entrada'   => Entrada::withAll()->findOrFail($id),
            'tags'      => Tag::all(),
        ]);
    }

    public function store(Request $request) {

        $request->validate([
            // 'valor' => 'required|unique:posts|max:255',
            'data' => 'required',
            'valor' => 'required|regex:/^[0-9]{1,3}([.]([0-9]{3}))*[,]([.]{0})[0-9]{0,2}$/',
            'descricao' => 'required',
            'tags' => 'required',
        ]);

        $valor = preg_replace("/[^0-9,]/", "", $request->input('valor'));
        $valor = str_replace(",", ".", $valor);

        $entrada = new Entrada();

        $entrada->data                = $request->input('data');
        $entrada->valor               = $valor;
        $entrada->descricao           = $request->input('descricao');
        $entrada->user_id             = Auth::id();

        $entrada->save();

        foreach ($request->input('tags') as $key => $value) {
            $entrada->tags()->attach($value);
        }

        return redirect('entrada')->with('status', 'Profile inserted!');
    }

    public function update(Request $request, $id) {

        $entrada = Entrada::findOrFail($id);

        $request->validate([
            // 'valor' => 'required|unique:posts|max:255',
            'data' => 'required',
            'valor' => 'required|regex:/^[0-9]{1,3}([.]([0-9]{3}))*[,]([.]{0})[0-9]{0,2}$/',
            'descricao' => 'required',
            'tags' => 'required',
        ]);

        $valor = preg_replace("/[^0-9,]/", "", $request->input('valor'));
        $valor = str_replace(",", ".", $valor);

        $entrada->data                = $request->input('data');
        $entrada->valor               = $valor;
        $entrada->descricao           = $request->input('descricao');
        $entrada->user_id             = Auth::id();

        $entrada->save();

        $entrada->tags()->detach();

        foreach ($request->input('tags') as $key => $value) {
            $entrada->tags()->attach($value);
        }

        return redirect('entrada')->with(['status' => 'Profile updated!', 'id' => $id]);
    }

    public function destroy($id) {

        $entrada = Entrada::findOrFail($id);
        $entrada->delete();

        return redirect('entrada')->with('status', 'Profile excluded!');
    }
}
