<?php

namespace App\Http\Controllers\Financeiro;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\Auth\User\User;
use App\Models\Banco;
use App\Models\CartaoBancario;
use App\Models\BandeiraCartao;
use App\Models\Saida;
use App\Models\FormaPagamento;
use App\Models\Tag;

class SaidaController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index() {

        // $saidas = Saida::withAll()
        //     ->orderBy('data', 'desc')
        //     ->orderBy('id', 'desc')
        //     ->where('user_id', Auth::id())
        //     // ->whereMonth('data', '=', date('m'))
        //     ->whereMonth('data', '=', 'may')
        //     ->get();

        // var_dump($saidas->toArray());
        // die;

        return view('financeiro.saida.index')->with([
            'saidas' => Saida::withAll()
            ->orderBy('data', 'desc')
            ->orderBy('id', 'desc')
            ->where('user_id', Auth::id())
            ->whereMonth('data', '=', date('05'))
            ->get(),
        ]);
    }

    public function create() {

        return view('financeiro.saida.cadastrar')->with([
            'formas_pagamento'  => FormaPagamento::all(),
            'tags'              => Tag::all(),
            'users'             => User::all()->except(Auth::id()),
        ]);
    }

    public function edit($id) {

        return view('financeiro.saida.cadastrar')->with([
            'saida'             => Saida::withAll()->findOrFail($id),
            'formas_pagamento'  => FormaPagamento::all(),
            'tags'              => Tag::all(),
            'users'             => User::all()->except(Auth::id()),
        ]);
    }

    // public function store(Request $request) {

    //     // var_dump($request->input('valores_individuais'));
    //     // die;

    //     // $only_numbers = preg_replace("/[^0-9,]/", "", $request->input('valor'));

    //     // $only_numbers = str_replace(",", ".", $only_numbers);

    //     // var_dump($only_numbers);
    //     // var_dump($request->input('valor'));
    //     // die;

    //     // $formatter = numfmt_create('pt_BR', NumberFormatter::CURRENCY);
    //     // var_dump(numfmt_parse_currency($formatter, $request->input('valor'), "BRL"));

    //     // $tt = preg_replace("/([^0-9\\.])/i", "", $request->input('valor'));
    //     $tt = $this->getAmount($request->input('valor'));

    //     $pp = number_format('1978.98', 2, ',', '.'); // retorna R$100.000,50

    //     // var_dump($pp);
    //     // die;

    //     $request->validate([
    //         // 'valor' => 'required|unique:posts|max:255',
    //         'data' => 'required',
    //         'valor' => 'required|regex:/^[0-9]{1,3}([.]([0-9]{3}))*[,]([.]{0})[0-9]{0,2}$/',
    //         'descricao' => 'required',
    //         'forma_pagamento' => 'required',
    //         'tags' => 'required',
    //     ]);

    //     $soma_valor_individual = 0;

    //     $valor = $this->formatar_valor($request->input('valor'));
    //     // $valor = $request->input('valor');
    //     // $valor = round($valor, 2);
    //     // $valor = str_replace(",", ".", $valor);

    //     if ( ! is_null($request->input('dividir_conta'))) {
    //         $qtnd_pessoas = count($request->input('users'));

    //         if ( ! is_null($request->input('dividir_conta_igualmente'))) {
    //             $valor_individual = $valor / ($qtnd_pessoas + 1);
    //             $valor_individual = round($valor_individual, 2);
    //             // $valor_individual = $this->formatar_valor($valor_individual);
    //             $valor_individual = str_replace(",", ".", $valor_individual);
    //         } else {
    //             foreach ($request->input('valores_individuais') as $ind) {

    //                 if (array_key_exists(Auth::id(), $ind)) {
    //                     $valor_individual = $ind[Auth::id()];

    //                     $valor_individual = str_replace(",", ".", $valor_individual);

    //                     $valor_individual = round($valor_individual, 2);

    //                     // var_dump($valor_individual);
    //                     // die;

    //                     // $valor_individual = number_format((float) $valor_individual, 2, '.', '');
    
    //                     // $valor_individual = $this->formatar_valor($valor_individual);
    //                     // $valor_individual = str_replace(",", ".", $valor_individual);
    //                 }

    //                 foreach ($ind as $key33 => $value33) {

    //                     $fff = str_replace(",", ".", $value33);

    //                     $fff = round($fff, 2);

    //                     $soma_valor_individual += $fff;
    //                 }

    
    //             }
    //         }
    //     }

    //     $soma_valor_individual = (float) $soma_valor_individual;
    //     $valor = (float) $valor;

    //     // var_dump( $soma_valor_individual);
    //     // var_dump( $valor_individual);
    //     // var_dump( $valor_individual);
    //     // var_dump( (float) $valor);
    //     // var_dump((float) $soma_valor_individual > (float) $valor);
    //     // die;

    //     if ($soma_valor_individual > $valor) {
    //         $request->validate([
    //             'valor' => 'required|numeric|max:-1',
    //         ]);
    //     }

    //     // var_dump($valor_individual);
    //     // die;

    //     $saida = new Saida();

    //     $saida->data                = $request->input('data');
    //     $saida->valor               = $valor;
    //     $saida->descricao           = $request->input('descricao');
    //     $saida->forma_pagamento_id  = $request->input('forma_pagamento');
    //     $saida->user_id             = Auth::id();

    //     if ( ! is_null($request->input('dividir_conta'))) {
    //         $saida->conta_dividida      = true;
    //         $saida->valor_individual    = $valor_individual;

    //         if ( ! is_null($request->input('dividir_conta_igualmente'))) {
    //             $saida->conta_dividida_igualmente = true;
    //         }
    //     }

    //     $saida->save();

    //     foreach ($request->input('tags') as $key => $value) {
    //         $saida->tags()->attach($value);
    //     }

    //     if ( ! empty($request->input('users'))) {
    //         foreach ($request->input('users') as $key => $value) {
    //             $saida->users()->attach($value);
    //         }
    //     }

    //     // Cria registro de saída para os usuários associados
    //     if ( ! empty($request->input('users'))) {
    //         foreach ($request->input('users') as $key => $value) {

    //             $saida = new Saida();

    //             $saida->data                = $request->input('data');
    //             $saida->valor               = $valor;
    //             $saida->descricao           = $request->input('descricao');
    //             // $saida->forma_pagamento_id  = $request->input('forma_pagamento');
    //             $saida->user_id             = $value;

    //             if ( ! is_null($request->input('dividir_conta'))) {
    //                 $qtnd_pessoas = count($request->input('users'));

    //                 if ( ! is_null($request->input('dividir_conta_igualmente'))) {
    //                     $valor_individual = $valor / ($qtnd_pessoas + 1);
    //                     $valor_individual = round($valor_individual, 2);
    //                     // $valor_individual = $this->formatar_valor($valor_individual);
    //                     $valor_individual = str_replace(",", ".", $valor_individual);
    //                 } else {
    //                     foreach ($request->input('valores_individuais') as $ind) {
    //                         if (array_key_exists($value, $ind)) {
    //                             // $valor_individual = $ind[$value];
    //                             // $valor_individual = round($valor_individual, 2);
    //                             // // $valor_individual = $this->formatar_valor($valor_individual);
    //                             // $valor_individual = str_replace(",", ".", $valor_individual);

    //                             $valor_individual = $ind[$value];

    //                             $valor_individual = str_replace(",", ".", $valor_individual);

    //                             $valor_individual = round($valor_individual, 2);
    //                         }
    //                     }
    //                 }
    //             }

    //             if ( ! is_null($request->input('dividir_conta'))) {
    //                 $saida->conta_dividida      = true;
    //                 $saida->valor_individual    = $valor_individual;

    //                 if ( ! is_null($request->input('dividir_conta_igualmente'))) {
    //                     $saida->conta_dividida_igualmente = true;
    //                 }
    //             }

    //             $saida->save();

    //             $saida->users()->attach(Auth::id());

    //         }
    //     }

    //     return redirect('financeiro')->with('status', 'Profile inserted!');
    // }

    public function store(Request $request) {

        // var_dump($request->input());
        // die;

        $request->validate([
            // 'valor' => 'required|unique:posts|max:255',
            'data' => 'required',
            'valor' => 'required|regex:/^[0-9]{1,3}([.]([0-9]{3}))*[,]([.]{0})[0-9]{0,2}$/',
            'descricao' => 'required',
            'forma_pagamento' => 'required',
            'tags' => 'required',
        ]);

        $valor = $this->formatar_valor($request->input('valor'));

        $valor = (float) $valor;

        $saida = new Saida();

        $saida->data                = $request->input('data');
        $saida->valor               = $valor;
        $saida->descricao           = $request->input('descricao');
        $saida->forma_pagamento_id  = $request->input('forma_pagamento');
        $saida->user_id             = Auth::id();

        $saida->save();

        foreach ($request->input('tags') as $key => $value) {
            $saida->tags()->attach($value);
        }

        return redirect('financeiro')->with('status', 'Profile inserted!');
    }

    public function update(Request $request, $id) {

        $saida = Saida::findOrFail($id);

        $request->validate([
            // 'valor' => 'required|unique:posts|max:255',
            'data' => 'required',
            'valor' => 'required|regex:/^[0-9]{1,3}([.]([0-9]{3}))*[,]([.]{0})[0-9]{0,2}$/',
            'descricao' => 'required',
            'forma_pagamento' => 'required',
            'tags' => 'required',
        ]);

        $valor = preg_replace("/[^0-9,]/", "", $request->input('valor'));
        $valor = str_replace(",", ".", $valor);

        if ( ! is_null($request->input('dividir_conta'))) {
            $valor /= 2;
        }

        $saida->data                = $request->input('data');
        $saida->valor               = $valor;
        $saida->descricao           = $request->input('descricao');
        $saida->forma_pagamento_id  = $request->input('forma_pagamento');
        $saida->user_id             = Auth::id();

        $saida->save();

        $saida->tags()->detach();
        // $saida->users()->detach();

        foreach ($request->input('tags') as $key => $value) {
            $saida->tags()->attach($value);
        }

        // if ( ! empty($request->input('users'))) {
        //     foreach ($request->input('users') as $key => $value) {
        //         $saida->users()->attach($value);
        //     }
        // }

        return redirect('financeiro')->with(['status' => 'Profile updated!', 'id' => $id]);
    }

    public function destroy($id) {

        $saida = Saida::findOrFail($id);
        $saida->delete();

        return redirect('financeiro')->with('status', 'Profile excluded!');
    }

    public function getAmount($money) {

        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

        return (float) str_replace(',', '.', $removedThousendSeparator);
    }

    public function ajax(Request $request) {

        $saidas = Saida::withAll()
        ->orderBy('data', 'desc')
        ->orderBy('id', 'desc')
        ->where('user_id', Auth::id())
        ->whereMonth('data', '=', $request->input('month'))
        ->get();

        $html = view('partials.table', compact('saidas'))->render();

        return response()->json(compact('html'));
    }

    public function dividir_conta_igualmente(Request $request) {

        $html = view('financeiro.saida.dividir_conta_igualmente')->render();

        return response()->json(compact('html'));
    }

    public function dividir_conta(Request $request) {

        $html = view('financeiro.saida.dividir_conta')->render();

        return response()->json(compact('html'));
    }

    public function input_individual(Request $request) {

        // var_dump($request->input('self') == "true");
        // die;

        if ($request->input('self') == "true") {
            $user = User::findOrFail(Auth::id());
        } else {
            $user = User::findOrFail($request->input('user_id'));
        }

        // print_r($user);
        // die;

        $html = view('financeiro.saida.input_individual', compact('user'))->render();

        return response()->json(compact('html'));
    }

    private function formatar_valor($valor){

        // var_dump($valor);
        // die;


        $valor = preg_replace("/[^0-9,]/", "", $valor);
        $valor = str_replace(",", ".", $valor);

        // var_dump($valor);
        // die;

        
        // var_dump($valor);
        // die;

        // $valor = number_format($valor,2,'.','');
        // var_dump($valor);
        // die;

        return $valor;
    }
}
