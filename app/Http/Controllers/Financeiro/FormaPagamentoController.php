<?php

namespace App\Http\Controllers\Financeiro;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\Banco;
use App\Models\CartaoBancario;
use App\Models\BandeiraCartao;
use App\Models\Saida;
use App\Models\FormaPagamento;
use App\Models\Tag;
use App\Models\ValeAlimentacaoRefeicao;

class FormaPagamentoController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index() {

    	return view('financeiro.formapagamento.index')->with([
            'formas_pagamento' => FormaPagamento::withAll()
            ->orderBy('id', 'desc')
            ->where('user_id', Auth::id())
            ->where('tipo', '!=', 'dinheiro')
            ->get(),
    	]);
    }

    public function create() {

    	return view('financeiro.formapagamento.cadastrar')->with([
    		'bancos'  => Banco::all(),
    		'bandeiras'  => BandeiraCartao::all(),
            // 'cartoes_bancarios'  => CartaoBancario::all(),
    	]);
    }

    public function edit($id) {

    	$forma_pagamento = FormaPagamento::findOrFail($id);
    	$forma_pagamento->vale_alimentacao_refeicao;
    	$forma_pagamento->cartao_bancario;
    	if ($forma_pagamento->cartao_bancario) {
    		$forma_pagamento->cartao_bancario->bandeira;
    	}

    	return view('financeiro.formapagamento.cadastrar')->with([
    		'forma_pagamento' => $forma_pagamento,
    		'bancos'  => Banco::all(),
    		'bandeiras'  => BandeiraCartao::all(),
    	]);
    }

    public function store(Request $request) {

    	$request->validate([
    		'tipo' => 'required',
    		'apelido' => 'required',
    	]);

    	if ($request->input('tipo') == "cartao_bancario") {

    		$request->validate([
    			'banco' => 'required',
    			'bandeira' => 'required',
    			'cartao_numero' => 'required',
    			'cartao_nome' => 'required',
    			'cartao_validade' => 'required',
    		]);
    	}

    	if ($request->input('tipo') == "vale_alimentação" || $request->input('tipo') == "vale_refeicao") {

    		$request->validate([
    			'ticket_numero' => 'required',
    			'ticket_nome' => 'required',
    			'ticket_validade' => 'required',
    			'ticket_empresa' => 'required',
    		]);
    	}

    	$forma_pagamento = new FormaPagamento();

    	$forma_pagamento->tipo                = $request->input('tipo');
    	$forma_pagamento->apelido      = $request->input('apelido');
        $forma_pagamento->user_id   = Auth::id();


    	if ($request->input('tipo') == "cartao_bancario") {

            $cartao_numero = str_replace(" ", "", $request->input('cartao_numero'));

    		$cartao_bancario = new CartaoBancario();

    		$cartao_bancario->banco_id            = $request->input('banco');
    		$cartao_bancario->bandeira_cartao_id  = $request->input('bandeira');
    		$cartao_bancario->numero              = $cartao_numero;
    		$cartao_bancario->nome                = $request->input('cartao_nome');
    		$cartao_bancario->validade            = $request->input('cartao_validade');

    		$cartao_bancario->save();

    		$forma_pagamento->cartao_bancario_id  = $cartao_bancario->id;
    	}

    	if ($request->input('tipo') == "vale_alimentação" || $request->input('tipo') == "vale_refeicao") {

    		$vale_alimentacao_refeicao = new ValeAlimentacaoRefeicao();

    		$vale_alimentacao_refeicao->numero                = $request->input('ticket_numero');
    		$vale_alimentacao_refeicao->nome                = $request->input('ticket_nome');
    		$vale_alimentacao_refeicao->validade                = $request->input('ticket_validade');
    		$vale_alimentacao_refeicao->empresa                = $request->input('ticket_empresa');

    		$vale_alimentacao_refeicao->save();

    		$forma_pagamento->vale_alimentacao_refeicao_id  = $vale_alimentacao_refeicao->id;
    	}

    	$forma_pagamento->save();

    	return redirect('formapagamento')->with('status', 'Profile inserted!');
    }

    public function update(Request $request, $id) {

    	$request->validate([
    		'tipo' => 'required',
    		'apelido' => 'required',
    	]);

    	if ($request->input('tipo') == "cartao_bancario") {

    		$request->validate([
    			'banco' => 'required',
    			'bandeira' => 'required',
    			'cartao_numero' => 'required',
    			'cartao_nome' => 'required',
    			'cartao_validade' => 'required',
    		]);
    	}

    	if ($request->input('tipo') == "vale_alimentação" || $request->input('tipo') == "vale_refeicao") {

    		$request->validate([
    			'ticket_numero' => 'required',
    			'ticket_nome' => 'required',
    			'ticket_validade' => 'required',
    			'ticket_empresa' => 'required',
    		]);
    	}

    	$forma_pagamento = FormaPagamento::findOrFail($id);

    	$forma_pagamento->tipo                = $request->input('tipo');
    	$forma_pagamento->apelido               = $request->input('apelido');
        $forma_pagamento->user_id   = Auth::id();


    	if ($request->input('tipo') == "cartao_bancario") {

    		if ($forma_pagamento->cartao_bancario) {
    			$cartao_bancario = CartaoBancario::findOrFail($forma_pagamento->cartao_bancario->id);
    		} else{
    			$cartao_bancario = new CartaoBancario();
    			$ticket = ValeAlimentacaoRefeicao::findOrFail($forma_pagamento->vale_alimentacao_refeicao->id);
    			$ticket->delete();
    		}

    		$cartao_bancario->banco_id                = $request->input('banco');
    		$cartao_bancario->bandeira_cartao_id                = $request->input('bandeira');
    		$cartao_bancario->numero                = $request->input('cartao_numero');
    		$cartao_bancario->nome                = $request->input('cartao_nome');
    		$cartao_bancario->validade                = $request->input('cartao_validade');

    		$cartao_bancario->save();

    		$forma_pagamento->cartao_bancario_id  = $cartao_bancario->id;
    	}

    	if ($request->input('tipo') == "vale_alimentação" || $request->input('tipo') == "vale_refeicao") {

    		if ($forma_pagamento->vale_alimentacao_refeicao) {
    			$ticket = ValeAlimentacaoRefeicao::findOrFail($forma_pagamento->vale_alimentacao_refeicao->id);
    		} else{
    			$ticket = new ValeAlimentacaoRefeicao();
    			$cartao_bancario = CartaoBancario::findOrFail($forma_pagamento->cartao_bancario->id);
    			$cartao_bancario->delete();
    		}


    		$ticket->numero                = $request->input('ticket_numero');
    		$ticket->nome                = $request->input('ticket_nome');
    		$ticket->validade                = $request->input('ticket_validade');
    		$ticket->empresa                = $request->input('ticket_empresa');

    		$ticket->save();

    		$forma_pagamento->vale_alimentacao_refeicao_id  = $ticket->id;
    	}

    	$forma_pagamento->save();

    	return redirect('formapagamento')->with('status', 'Profile updated!');
    }

    public function destroy($id) {

    	$forma_pagamento = FormaPagamento::findOrFail($id);

    	$saida = Saida::where('forma_pagamento_id', $forma_pagamento->id)->get();

    	if ( ! $saida->isEmpty()) {
    		return redirect('formapagamento')
    		->with('error', 'Forma de pagamento está sendo utilizada por uma ou mais saídas!');
    	}

    	$forma_pagamento->delete();

    	return redirect('formapagamento')->with('status', 'Profile excluded!');
    }
}
