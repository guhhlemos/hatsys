<?php

namespace App\Http\Controllers\Financeiro;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\User;
use App\Models\Banco;
use App\Models\CartaoBancario;
use App\Models\BandeiraCartao;
use App\Models\Saida;
use App\Models\Entrada;
use App\Models\FormaPagamento;
use App\Models\Tag;

class HistoricoController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index() {

        return view('financeiro.historico.index');
        // return view('financeiro.historico.index')->with([
        //     'saidas' => Saida::withAll()
        //     ->orderBy('data', 'desc')
        //     ->where('user_id', Auth::id())
        //     ->whereMonth('data', '=', date('m'))
        //     ->get(),
        // ]);
    }

    public function create() {

        return view('financeiro.historico.cadastrar')->with([
            'formas_pagamento'  => FormaPagamento::all(),
            'tags'              => Tag::all(),
            'users'             => User::all(),
        ]);
    }

    public function edit($id) {

        return view('financeiro.historico.cadastrar')->with([
            'saida'             => Saida::withAll()->findOrFail($id),
            'formas_pagamento'  => FormaPagamento::all(),
            'tags'              => Tag::all(),
            'users'             => User::all(),
        ]);
    }

    public function store(Request $request) {

        // $only_numbers = preg_replace("/[^0-9,]/", "", $request->input('valor'));

        // $only_numbers = str_replace(",", ".", $only_numbers);

        // var_dump($only_numbers);
        // var_dump($request->input('valor'));
        // die;

        // $formatter = numfmt_create('pt_BR', NumberFormatter::CURRENCY);
        // var_dump(numfmt_parse_currency($formatter, $request->input('valor'), "BRL"));

        // $tt = preg_replace("/([^0-9\\.])/i", "", $request->input('valor'));
        $tt = $this->getAmount($request->input('valor'));

        $pp = number_format('1978.98', 2, ',', '.'); // retorna R$100.000,50

        // var_dump($pp);
        // die;

        $request->validate([
            // 'valor' => 'required|unique:posts|max:255',
            'data' => 'required',
            'valor' => 'required|regex:/^[0-9]{1,3}([.]([0-9]{3}))*[,]([.]{0})[0-9]{0,2}$/',
            'descricao' => 'required',
            'forma_pagamento' => 'required',
            'tags' => 'required',
        ]);

        $valor = preg_replace("/[^0-9,]/", "", $request->input('valor'));
        $valor = str_replace(",", ".", $valor);

        $saida = new Saida();

        $saida->data                = $request->input('data');
        $saida->valor               = $valor;
        $saida->descricao           = $request->input('descricao');
        $saida->forma_pagamento_id  = $request->input('forma_pagamento');
        $saida->user_id             = Auth::id();

        $saida->save();

        foreach ($request->input('tags') as $key => $value) {
            $saida->tags()->attach($value);
        }

        if ( ! empty($request->input('users'))) {
            foreach ($request->input('users') as $key => $value) {
                $saida->users()->attach($value);
            }
        }

        return redirect('financeiro')->with('status', 'Profile inserted!');
    }

    public function update(Request $request, $id) {

        $saida = Saida::findOrFail($id);

        $request->validate([
            // 'valor' => 'required|unique:posts|max:255',
            'data' => 'required',
            'valor' => 'required|regex:/^[0-9]{1,3}([.]([0-9]{3}))*[,]([.]{0})[0-9]{0,2}$/',
            'descricao' => 'required',
            'forma_pagamento' => 'required',
            'tags' => 'required',
        ]);

        $valor = preg_replace("/[^0-9,]/", "", $request->input('valor'));
        $valor = str_replace(",", ".", $valor);

        $saida->data                = $request->input('data');
        $saida->valor               = $valor;
        $saida->descricao           = $request->input('descricao');
        $saida->forma_pagamento_id  = $request->input('forma_pagamento');

        $saida->save();

        $saida->tags()->detach();
        $saida->users()->detach();

        foreach ($request->input('tags') as $key => $value) {
            $saida->tags()->attach($value);
        }

        if ( ! empty($request->input('users'))) {
            foreach ($request->input('users') as $key => $value) {
                $saida->users()->attach($value);
            }
        }

        return redirect('financeiro')->with(['status' => 'Profile updated!', 'id' => $id]);
    }

    public function destroy($id) {

        $saida = Saida::findOrFail($id);
        $saida->delete();

        return redirect('financeiro')->with('status', 'Profile excluded!');
    }

    public function getAmount($money) {

        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

        return (float) str_replace(',', '.', $removedThousendSeparator);
    }

    public function ajax(Request $request) {

        if ($request->input('tipo') == 'saida') {

            $saidas = Saida::withAll()
            ->orderBy('data', 'desc')
            ->orderBy('id', 'desc')
            ->where('user_id', Auth::id())
            ->whereMonth('data', '=', $request->input('month'))
            ->get();

            $html = view('partials.table', compact('saidas'))->render();

            return response()->json(compact('html'));

        } elseif ($request->input('tipo') == 'entrada') {

            $entradas = Entrada::withAll()
            ->orderBy('data', 'desc')
            ->orderBy('id', 'desc')
            ->where('user_id', Auth::id())
            ->whereMonth('data', '=', $request->input('month'))
            ->get();

            $html = view('partials.table_entrada', compact('entradas'))->render();

            return response()->json(compact('html'));
        }

        
    }
}
