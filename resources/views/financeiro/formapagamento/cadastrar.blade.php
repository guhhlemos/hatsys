@extends('layouts.application')

@section('content')

<script>

    $(function () {

        $('#tipo').on('change', function() {
            var value = $(this).find(":selected").val();

            console.log(value);

            $('#vale_refeicao_alimentacao').hide();
            $('#cartao_bancario').hide();

            if (value == "cartao_bancario") {
                $('#cartao_bancario').show(200);
            }
            if (value == "vale_alimentação") {
                $('#vale_refeicao_alimentacao').show(200);
            }
            if (value == "vale_refeicao") {
                $('#vale_refeicao_alimentacao').show(200);
            }
        });

        $("#cartao_numero").inputmask({"mask" : "9999 9999 9999 9999"});



        var forma_pagamento = null;

        @isset($forma_pagamento)
        forma_pagamento = @json($forma_pagamento);
        @endisset

        console.log(forma_pagamento);

        if (forma_pagamento) {

            // infra_selected('#tipo', forma_pagamento.placa_mae_id);

            $('#tipo').val(forma_pagamento.tipo).change();

            $('#apelido').val(forma_pagamento.apelido);

            

            if (forma_pagamento.cartao_bancario) {

                $("#banco > option").each(function() {
                    if (this.value == forma_pagamento.cartao_bancario.id) {
                        $(this).attr("selected","selected");
                    } else {
                        $(this).removeAttr("selected");
                    }
                });

                $("#bandeira > option").each(function() {
                    if (this.value == forma_pagamento.cartao_bancario.bandeira.id) {
                        $(this).attr("selected","selected");
                    } else {
                        $(this).removeAttr("selected");
                    }
                });

                $('#cartao_numero').val(forma_pagamento.cartao_bancario.numero);
                $('#cartao_nome').val(forma_pagamento.cartao_bancario.nome);
                $('#cartao_validade').val(forma_pagamento.cartao_bancario.validade);
            }

            if (forma_pagamento.vale_alimentacao_refeicao) {
                $('#ticket_numero').val(forma_pagamento.vale_alimentacao_refeicao.numero);
                $('#ticket_nome').val(forma_pagamento.vale_alimentacao_refeicao.nome);
                $('#ticket_validade').val(forma_pagamento.vale_alimentacao_refeicao.validade);
                $('#ticket_empresa').val(forma_pagamento.vale_alimentacao_refeicao.empresa);
            }


        } else {
            // $('#tipo').val(new Date("yyyy-MM-dd"));
            // $("#tipo").val( moment().format('YYYY-MM-DD') );
        }


    });
    
</script>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h2 class="titulo">Forma Pagamento - <?php echo isset($forma_pagamento) ? 'Alterar' : 'Cadastrar' ?></h2>

            <form method="POST">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('tipo') ? ' has-error' : '' }}">
                    <label for="tipo">Tipo</label>
                    <select name="tipo" class="form-control" id="tipo">
                        <option value="cartao_bancario">Cartão Bancário</option>
                        <option value="vale_alimentação">Vale Alimentação</option>
                        <option value="vale_refeicao">Vale Refeição</option>
                    </select>
                    @if ($errors->has('tipo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('tipo') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('apelido') ? ' has-error' : '' }}">
                    <label for="apelido">Apelido</label>
                    <input font-family="monospace" name="apelido" type="text" class="form-control" id="apelido" placeholder="Apelido">
                    @if ($errors->has('apelido'))
                    <span class="help-block">
                        <strong>{{ $errors->first('apelido') }}</strong>
                    </span>
                    @endif
                </div>






                <div id="cartao_bancario">
                    <div class="form-group{{ $errors->has('banco') ? ' has-error' : '' }}">
                        <label for="banco">Banco</label>
                        <select name="banco" class="form-control" id="banco">
                            @foreach ($bancos as $banco)
                            <option value="{{ $banco->id }}">{{ $banco->nome }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('banco'))
                        <span class="help-block">
                            <strong>{{ $errors->first('banco') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('bandeira') ? ' has-error' : '' }}">
                        <label for="bandeira">Bandeira</label>
                        <select name="bandeira" class="form-control" id="bandeira">
                            @foreach ($bandeiras as $bandeira)
                            <option value="{{ $bandeira->id }}">{{ $bandeira->nome }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('bandeira'))
                        <span class="help-block">
                            <strong>{{ $errors->first('bandeira') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('cartao_numero') ? ' has-error' : '' }}">
                        <label for="cartao_numero">Número</label>
                        <input font-family="monospace" name="cartao_numero" type="text" class="form-control" id="cartao_numero" placeholder="Número">
                        @if ($errors->has('cartao_numero'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cartao_numero') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('cartao_nome') ? ' has-error' : '' }}">
                        <label for="cartao_nome">Nome</label>
                        <input font-family="monospace" name="cartao_nome" type="text" class="form-control" id="cartao_nome" placeholder="Nome">
                        @if ($errors->has('cartao_nome'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cartao_nome') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('cartao_validade') ? ' has-error' : '' }}">
                        <label for="cartao_validade">Validade</label>
                        <input font-family="monospace" name="cartao_validade" type="date" class="form-control" id="cartao_validade" placeholder="Validade">
                        @if ($errors->has('cartao_validade'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cartao_validade') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div id="vale_refeicao_alimentacao" style="display: none;">
                    <div class="form-group{{ $errors->has('ticket_numero') ? ' has-error' : '' }}">
                        <label for="ticket_numero">Número</label>
                        <input font-family="monospace" name="ticket_numero" type="text" class="form-control" id="ticket_numero" placeholder="Número">
                        @if ($errors->has('ticket_numero'))
                        <span class="help-block">
                            <strong>{{ $errors->first('ticket_numero') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('ticket_nome') ? ' has-error' : '' }}">
                        <label for="ticket_nome">Nome</label>
                        <input font-family="monospace" name="ticket_nome" type="text" class="form-control" id="ticket_nome" placeholder="Nome">
                        @if ($errors->has('ticket_nome'))
                        <span class="help-block">
                            <strong>{{ $errors->first('ticket_nome') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('ticket_validade') ? ' has-error' : '' }}">
                        <label for="ticket_validade">Validade</label>
                        <input font-family="monospace" name="ticket_validade" type="date" class="form-control" id="ticket_validade" placeholder="Validade">
                        @if ($errors->has('ticket_validade'))
                        <span class="help-block">
                            <strong>{{ $errors->first('ticket_validade') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('ticket_empresa') ? ' has-error' : '' }}">
                        <label for="ticket_empresa">Empresa</label>
                        <input font-family="monospace" name="ticket_empresa" type="text" class="form-control" id="ticket_empresa" placeholder="Empresa">
                        @if ($errors->has('ticket_empresa'))
                        <span class="help-block">
                            <strong>{{ $errors->first('ticket_empresa') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ url('/formapagamento') }}" class="btn btn-danger" role="button">Cancelar</a>
            </form>

        </div>
    </div>
</div>
@endsection