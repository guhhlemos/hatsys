@extends('layouts.application')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h2 class="titulo">Forma pagamento - Index</h2>

            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif

            @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
            @endif

            <a href="{{ url('/formapagamento/cadastrar') }}" class="btn btn-primary" role="button" style="float: right;">Cadastrar</a>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Apelido</th>
                        <th scope="col">Cartão Bancário</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($formas_pagamento as $forma_pagamento)
                    <tr>
                        <th scope="row">{{ $forma_pagamento->id }}</th>
                        <td>{{ $forma_pagamento->tipo }}</td>
                        <td>{{ $forma_pagamento->apelido }}</td>
                        <td>@isset($forma_pagamento->cartao_bancario){{ $forma_pagamento->cartao_bancario->numero }}@endisset</td>
                        <td><a href="{{ url('/formapagamento/editar') }}/{{ $forma_pagamento->id }}" class="btn btn-primary" role="button">Alterar</a></td>
                        <td>
                            <form method="POST" action="{{ url('/formapagamento/excluir') }}/{{ $forma_pagamento->id }}">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger">Excluir</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
@endsection