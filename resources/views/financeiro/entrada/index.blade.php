@extends('layouts.application')

@section('content')

<script>
    $(function () {

        var entradas = null;

        @isset($entradas)
        entradas = @json($entradas);
        @endisset

        console.log(entradas);

        $(".price").inputmask('currency', {
            'prefix': 'R$ ',
            'rightAlign': false,
            'groupSeparator': '.',
            'radixPoint': ",",
        });

        var total = 0;

        $.each(entradas, function(key, entrada){
            total += parseFloat(entrada.valor);
        });

        total = total.toFixed(2);

        $('#total').text(total);

        @if (session('status'))
        $("#row_{{ session('id') }}").css("background-color", "yellow");
        @endif


    });
</script>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h2 class="titulo">Financeiro - Entrada - Index</h2>

            @if (session('status'))
            <div class="alert alert-success" id="mensagem_status">
                {{ session('id') }}: {{ session('status') }}
            </div>
            @endif

            <h4>TOTAL: <p id="total"></p></h4>

            <a href="{{ url('/entrada/cadastrar') }}" class="btn btn-success" role="button" style="float: right;">Cadastrar</a>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Data</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Descrição</th>
                        <th scope="col">Tags</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($entradas as $entrada)
                    <tr id="row_{{ $entrada->id }}">
                        <th scope="row">{{ $entrada->id }}</th>
                        <td class="date">{{ $entrada->data }}</td>
                        <td class="price">{{ $entrada->valor }}</td>
                        <td>{{ $entrada->descricao }}</td>
                        <td>
                            @foreach ($entrada->tags as $tag)
                            <p>{{ $tag->nome }}</p>
                            @endforeach
                        </td>
                        <td><a href="{{ url('/entrada/editar') }}/{{ $entrada->id }}" class="btn btn-primary" role="button">Alterar</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
@endsection