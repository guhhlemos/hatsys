@extends('layouts.application')

@section('content')

<script>

    $(function () {

        $("#valor").inputmask('currency', {
            // 'prefix': 'R$ ',
            'prefix': '',
            'rightAlign': false,
            'groupSeparator': '.',
            'radixPoint': ",",
        });

        // $("#valor").inputmask('decimal', {
        //     'alias': 'numeric',
        //     'groupSeparator': '.',
        //     'autoGroup': true,
        //     'digits': 2,
        //     'radixPoint': ",",
        //     'digitsOptional': false,
        //     'allowMinus': false,
        //     // 'prefix': 'R$ ',
        //     'placeholder': '',
        //     'rightAlign': false,
        // });

        var entrada = null;

        @isset($entrada)
        entrada = @json($entrada);
        @endisset

        console.log(entrada);

        if (entrada) {

            $('#data').val(entrada.data);
            $('#valor').val(entrada.valor);
            $('#descricao').val(entrada.descricao);

            var tags = [];

            $.each(entrada.tags, function(key, tag) {
                tags.push(tag.id);
            });

            $("#tags").val(tags);

            
        } else {
            // $('#data').val(new Date("yyyy-MM-dd"));
            $("#data").val( moment().format('YYYY-MM-DD') );
        }

    });
    
</script>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h2 class="titulo">Financeiro - <?php echo isset($entrada) ? 'Alterar' : 'Cadastrar' ?></h2>

            <form id="form1" method="POST">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('data') ? ' has-error' : '' }}">
                    <label for="data">Data</label>
                    <input name="data" type="date" class="form-control" id="data" placeholder="28/02/2018">
                    @if ($errors->has('data'))
                    <span class="help-block">
                        <strong>{{ $errors->first('data') }}</strong>
                    </span>
                    @endif
                </div>



                <div class="form-group{{ $errors->has('valor') ? ' has-error' : '' }}">
                    <label for="valor">Valor</label>
                    <div class="input-group">
                        <span class="input-group-addon">R$</span>
                        <input font-family="monospace" name="valor" type="text" class="form-control" id="valor" placeholder="12.87">
                    </div>
                    @if ($errors->has('valor'))
                    <span class="help-block">
                        <strong>{{ $errors->first('valor') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
                    <label for="descricao">Descrição</label>
                    <input name="descricao" type="text" class="form-control" id="descricao" placeholder="Descrição da compra realizada" required>
                    @if ($errors->has('descricao'))
                    <span class="help-block">
                        <strong>{{ $errors->first('descricao') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                    <label for="tags">Tags</label>
                    <select multiple name="tags[]" class="form-control" id="tags" required style="height: 160px;">
                        @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}">{{ $tag->nome }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('tags'))
                    <span class="help-block">
                        <strong>{{ $errors->first('tags') }}</strong>
                    </span>
                    @endif
                </div>
            </form>

            <button type="submit" class="btn btn-primary" form="form1">Salvar</button>
            <a href="{{ url('/entrada') }}" class="btn btn-danger" role="button">Cancelar</a>
            @isset($entrada)

            <form id="form2" method="POST" action="{{ url('/entrada/excluir') }}/{{ $entrada->id }}">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger" form="form2">Excluir</button>
            </form>
            @endisset

        </div>
    </div>
</div>
@endsection