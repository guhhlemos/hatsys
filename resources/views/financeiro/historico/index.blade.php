@extends('layouts.application')

@section('content')

<script>
    $(function () {

        var saidas = null;

        @isset($saidas)
        saidas = @json($saidas);
        @endisset

        console.log(saidas);

        $(".price").inputmask('currency', {
            'prefix': 'R$ ',
            'rightAlign': false,
            'groupSeparator': '.',
            'radixPoint': ",",
        });

        $('#buscar').on('click', function(){

            // var date = new Date($('#date').val());
            // day = date.getDate();
            // month = date.getMonth() + 2;
            // year = date.getFullYear();

            var date = $('#date').val().split('-');
            var year = date[2];
            var month = date[1];
            var day = date[0];


            // var date = moment($('#date').val());
            // var day = moment(date).date();
            // var month = moment(date).month();
            // var year = moment(date).year();

            // console.log(date);
            // console.log(today);
            // console.log(tomorrow);

            console.log(day);
            console.log(month);
            console.log(year);

            var tipo = $("input[name='inlineRadioOptions']:checked").val();
            console.log(tipo);

            renderTable("{{ url('/historico/ajax') }}", month, tipo);
        });

        $('#date').on('change', function(){

            var date = $('#date').val().split('-');
            var year = date[2];
            var month = date[1];
            var day = date[0];

            var tipo = $("input[name='inlineRadioOptions']:checked").val();
            console.log(tipo);

            renderTable("{{ url('/historico/ajax') }}", month, tipo);
        });

        

    });
</script>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h2 class="titulo">Financeiro - Histórico - Index</h2>

            @if (session('status'))
            <div class="alert alert-success" id="mensagem_status">
                {{ session('id') }}: {{ session('status') }}
            </div>
            @endif

            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="entrada"> Entrada
            </label>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="saida" checked> Saída
            </label>

            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" id="buscar">Go!</button>
                        </span>
                        <input name="date" type="month" class="form-control" id="date" placeholder="28/02/2018">
                    </div>
                </div>
            </div>

            <div class="table-container">
                @include('partials.table')
            </div>

        </div>
    </div>
</div>
@endsection