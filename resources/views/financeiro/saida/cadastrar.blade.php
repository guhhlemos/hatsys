@extends('layouts.application')

@section('content')

<script>

    $(function () {

        // dividir_conta();

        $(".valor").inputmask('currency', {
            // 'prefix': 'R$ ',
            'prefix': '',
            'rightAlign': false,
            'groupSeparator': '.',
            'radixPoint': ",",
        });

        var saida = null;

        @isset($saida)
        saida = @json($saida);
        @endisset

        console.log(saida);

        var logged_user = @json(Auth::user());

        console.log(logged_user);

        if (saida) {

            $('#data').val(saida.data);
            $('#valor').val(saida.valor);
            $('#descricao').val(saida.descricao);
            $("#forma_pagamento").val(saida.forma_pagamento.id);

            $("#tags").val(getObjectIds(saida.tags));
            // $("#users").val(getObjectIds(saida.users));
            $("#users").val(getObjectUsersIds(saida.childs));

        } else {
            $("#data").val( moment().format('YYYY-MM-DD') );
        }

    });

</script>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h2 class="titulo"><?php echo isset($saida) ? 'Alterar' : 'Cadastrar' ?></h2>

            <form id="form1" method="POST">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('data') ? ' has-error' : '' }}">
                    <label for="data">Data</label>
                    <input name="data" type="date" class="form-control" id="data" placeholder="28/02/2018">
                    @if ($errors->has('data'))
                    <span class="help-block">
                        <strong>{{ $errors->first('data') }}</strong>
                    </span>
                    @endif
                </div>



                <div class="form-group{{ $errors->has('valor') ? ' has-error' : '' }}">
                    <label for="valor">Valor total</label>
                    <div class="input-group">
                        <span class="input-group-addon">R$</span>
                        <input font-family="monospace" name="valor" type="text" class="form-control valor" id="valor" placeholder="12.87">
                    </div>
                    @if ($errors->has('valor'))
                    <span class="help-block">
                        <strong>{{ $errors->first('valor') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
                    <label for="descricao">Descrição</label>
                    <input name="descricao" type="text" class="form-control" id="descricao" placeholder="Descrição da compra realizada" required>
                    @if ($errors->has('descricao'))
                    <span class="help-block">
                        <strong>{{ $errors->first('descricao') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('forma_pagamento') ? ' has-error' : '' }}">
                    <label for="forma_pagamento">Forma de pagamento</label>
                    <select name="forma_pagamento" class="form-control" id="forma_pagamento" required>
                        @foreach ($formas_pagamento as $forma_pagamento)
                        <option value="{{ $forma_pagamento->id }}">{{ $forma_pagamento->apelido }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('forma_pagamento'))
                    <span class="help-block">
                        <strong>{{ $errors->first('forma_pagamento') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                    <label for="tags">Tags</label>
                    <select multiple name="tags[]" class="form-control" id="tags" required style="height: 160px;">
                        @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}">{{ $tag->nome }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('tags'))
                    <span class="help-block">
                        <strong>{{ $errors->first('tags') }}</strong>
                    </span>
                    @endif
                </div>
            </form>

            <button type="submit" class="btn btn-primary" form="form1">Salvar</button>
            <a href="{{ url('/financeiro') }}" class="btn btn-danger" role="button">Cancelar</a>
            @isset($saida)

            <form id="form2" method="POST" action="{{ url('/financeiro/excluir') }}/{{ $saida->id }}">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger" form="form2">Excluir</button>
            </form>
            @endisset

        </div>
    </div>
</div>
@endsection