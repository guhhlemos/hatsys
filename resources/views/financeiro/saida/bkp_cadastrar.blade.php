@extends('layouts.app')
@section('title', 'Financeiro')
@section('content')

<script>

    $(function () {

        // dividir_conta();

        $(".valor").inputmask('currency', {
            // 'prefix': 'R$ ',
            'prefix': '',
            'rightAlign': false,
            'groupSeparator': '.',
            'radixPoint': ",",
        });

        // $(document).on('change', function(){

        //     console.log('teste');

        //     $(".valor").inputmask('currency', {
        //         // 'prefix': 'R$ ',
        //         'prefix': '',
        //         'rightAlign': false,
        //         'groupSeparator': '.',
        //         'radixPoint': ",",
        //     });
        // });

        

        // $("#valor").inputmask('decimal', {
        //     'alias': 'numeric',
        //     'groupSeparator': '.',
        //     'autoGroup': true,
        //     'digits': 2,
        //     'radixPoint': ",",
        //     'digitsOptional': false,
        //     'allowMinus': false,
        //     // 'prefix': 'R$ ',
        //     'placeholder': '',
        //     'rightAlign': false,
        // });

        var saida = null;

        @isset($saida)
        saida = @json($saida);
        @endisset

        console.log(saida);

        var logged_user = @json(Auth::user());

        console.log(logged_user);

        if (saida) {

            $('#data').val(saida.data);
            $('#valor').val(saida.valor);
            $('#descricao').val(saida.descricao);
            $("#forma_pagamento").val(saida.forma_pagamento.id);

            $("#tags").val(getObjectIds(saida.tags));
            // $("#users").val(getObjectIds(saida.users));
            $("#users").val(getObjectUsersIds(saida.childs));

            if (saida.conta_dividida) {
                $("#dividir_conta").attr("checked","checked").change();
            }
            
        } else {
            // $('#data').val(new Date("yyyy-MM-dd"));
            $("#data").val( moment().format('YYYY-MM-DD') );
        }

        // **************************************************************************************
        // **************************************************************************************

        $("#dividir_conta").on("change", function(){

            console.log("#dividir_conta: " + $("#dividir_conta").is(':checked'));

            $("#dividir_conta_info").empty();

            if ($("#dividir_conta").is(':checked')) {

                $("#input_dividir_conta_igualmente").show();
                $("#dividir_conta_igualmente").change();

                if ($("#dividir_conta_igualmente").is(':checked')) {

                    var qntd_pessoas = $("#users option:selected").length + 1;

                    var valor = ($('#valor').val());
                    valor = valor.replace(",", ".");
                    valor = parseFloat(valor);

                    var valor_dividido = ($('#valor').val() == 0) ? 0 : ((valor) / (qntd_pessoas)).toFixed(2);

                    // console.log($('#valor').val());
                    // console.log(valor);
                    // console.log(valor_dividido);

                    var texto = $("<p>").text("Você fica com R$" + valor_dividido + " da conta");

                    $("#dividir_conta_info").append(texto);

                    $("#users option:selected").each(function(){
                        var usuario = $(this).text();

                        var texto = $("<p>").text(usuario + " fica com R$" + valor_dividido + " da conta");

                        $("#dividir_conta_info").append(texto);
                    });

                } else {

                    var texto = $("<p>").text("teste de texto");

                    $("#dividir_conta_info").append(texto);

                    
                }

            } else {
                $("#input_dividir_conta_igualmente").hide();
                $("#dividir_conta_igualmente").change();
                $("#dividir_conta_igualmente").prop("checked", true).change();
            }
        });

        $("#dividir_conta_igualmente").on("change", function(){
            // console.log(this.checked);

            console.log("#dividir_conta_igualmente: " + $("#dividir_conta_igualmente").is(':checked'));

            var div_clone = ".input_individual";

            if ( $(div_clone).length > 1 ) {
                $(div_clone).not(":first").remove();
            }

            if ($("#dividir_conta_igualmente").is(':checked')) {

                // input_for_individual_user_value(div_clone, logged_user.name, logged_user.id);

                // $("#users option:selected").each(function(){

                //     var usuario = $(this).text();
                //     var usuario_id = $(this).val();

                //     input_for_individual_user_value(div_clone, usuario, usuario_id);

                // });

                $(div_clone).not(":first").remove();

            } else {

                valor_individual = (saida && saida.conta_dividida && saida.conta_dividida_igualmente) ? saida.valor_individual : null;

                input_for_individual_user_value(div_clone, logged_user.name, logged_user.id, valor_individual);

                $("#users option:selected").each(function(){

                    var usuario = $(this).text();
                    var usuario_id = $(this).val();

                    if (saida && saida.conta_dividida && saida.conta_dividida_igualmente) {
                        $.each(saida.childs, function(key, value) {

                            if (value.user.id == usuario_id) {
                                input_for_individual_user_value(div_clone, usuario, usuario_id, value.valor_individual);
                            }

                        });
                    }

                    

                });

                // $("#input_dividir_conta_igualmente").hide();
                // $("#dividir_conta_igualmente").change();
                // $(div_clone).not(":first").remove();
                // $("#dividir_conta_igualmente").prop("checked", false);
            }

            if ($("#users option:selected").length == 0) {
                // $("#input_dividir_conta_igualmente").hide();
                // $(".input_individual").hide();
                // $(div_clone).not(":first").remove();
                // $("#dividir_conta_igualmente").prop("checked", false);
                return;
            }

        });
        

        $("#users").on('change', function(){

            console.log("#users: " + $("#users option:selected").length);

            

            if ($("#users option:selected").length > 0) {
                $("#input_dividir_conta").show();
                $("#dividir_conta").change();
                // $("#dividir_conta").attr("checked","checked");
            } else {
                $("#input_dividir_conta").hide();
                $("#dividir_conta").prop("checked", false).change();
                $("#dividir_conta_igualmente").prop("checked", true).change();
            }

        });

        $('#valor').on('input focusout blur', function(){
            $("#dividir_conta").change();
        });

        // $('#users').on('change', function(){
        //     $("#dividir_conta").change();
        // });

        // $("#dividir_conta").change();
        $("#users").change();

        $(document).on("change", ".valores_individuais", function(){

            console.log('-----------');

            // console.log($(this).val());

            var b = $(this).val();
            b = b.replace(",", ".");
            b = parseFloat(b);



            var valor_total = ($('#valor').val());
            valor_total = valor_total.replace(",", ".");
            valor_total = parseFloat(valor_total);

            // console.log(b);
            // console.log(valor_total);
            // console.log(b > valor_total);
            // return;

            // if (b > valor_total) {
            //     return;
            // }

            var empty_fields = [];
            var not_empty_fields = [];

            $(".valores_individuais").not(":first").each(function(){

                // console.log($(this).val() == "" || $(this).val() == "0,00");
                // console.log($(this).val());

                if ($(this).val() == "" || $(this).val() == "0,00") {
                    empty_fields.push($(this));
                } else {
                    not_empty_fields.push($(this));
                }
            });

            // console.log(empty_fields);
            // console.log(not_empty_fields);
            // return;

            

            var v1 = valor_total;

            $(not_empty_fields).each(function(){

                var a = $(this).val();
                a = a.replace(",", ".");
                a = parseFloat(a);
                
                v1 -= a;
                v1 = v1.toFixed(2);
                v1 = parseFloat(v1);
            });

            // console.log(empty_fields);
            // console.log(not_empty_fields);
            // return;

            console.log(v1);

            $(empty_fields).each(function(){
                // $(this).val(valor_dividido);

                // console.log($(this));

                $(this).inputmask('remove');

                $(this).inputmask('currency', {
                    // 'prefix': 'R$ ',
                    'prefix': '',
                    'rightAlign': false,
                    'groupSeparator': '.',
                    'radixPoint': ",",
                    'max': v1,
                });

            });


            $(not_empty_fields).each(function(){
                // $(this).val(valor_dividido);

                // console.log($(this));

                var c = ($(this).val());
                c = c.replace(",", ".");
                c = parseFloat(c);
                c = c.toFixed(2);
                c = parseFloat(c);

                c += v1;
                c = parseFloat(c);

                $(this).inputmask('remove');

                $(this).inputmask('currency', {
                    // 'prefix': 'R$ ',
                    'prefix': '',
                    'rightAlign': false,
                    'groupSeparator': '.',
                    'radixPoint': ",",
                    'max': c,
                });

            });

            // $("#dividir_conta").change();


        });

    });

</script>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <h2 class="titulo">Financeiro - <?php echo isset($saida) ? 'Alterar' : 'Cadastrar' ?></h2>

            <form id="form1" method="POST">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('data') ? ' has-error' : '' }}">
                    <label for="data">Data</label>
                    <input name="data" type="date" class="form-control" id="data" placeholder="28/02/2018">
                    @if ($errors->has('data'))
                    <span class="help-block">
                        <strong>{{ $errors->first('data') }}</strong>
                    </span>
                    @endif
                </div>



                <div class="form-group{{ $errors->has('valor') ? ' has-error' : '' }}">
                    <label for="valor">Valor total</label>
                    <div class="input-group">
                        <span class="input-group-addon">R$</span>
                        <input font-family="monospace" name="valor" type="text" class="form-control valor" id="valor" placeholder="12.87">
                    </div>
                    @if ($errors->has('valor'))
                    <span class="help-block">
                        <strong>{{ $errors->first('valor') }}</strong>
                    </span>
                    @endif
                </div>

                <div id="input_dividir_conta">
                    <div class="form-group{{ $errors->has('dividir_conta') ? ' has-error' : '' }}">
                        <label for="dividir_conta">Dividir conta com a galera?</label>
                        <input name="dividir_conta" type="checkbox" id="dividir_conta">
                        @if ($errors->has('dividir_conta'))
                        <span class="help-block">
                            <strong>{{ $errors->first('dividir_conta') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div id="input_dividir_conta_igualmente">
                    <div class="form-group{{ $errors->has('dividir_conta_igualmente') ? ' has-error' : '' }}">
                        <label for="dividir_conta_igualmente">Dividir conta igualmente?</label>
                        <input name="dividir_conta_igualmente" type="checkbox" id="dividir_conta_igualmente">
                        @if ($errors->has('dividir_conta_igualmente'))
                        <span class="help-block">
                            <strong>{{ $errors->first('dividir_conta_igualmente') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="input_individual" style="display: none;">
                    <div class="form-group{{ $errors->has('valores_individuais') ? ' has-error' : '' }}">
                        <label for="valores_individuais">Valor Jon</label>
                        <div class="input-group">
                            <span class="input-group-addon">R$</span>
                            <input font-family="monospace" type="text" class="form-control valores_individuais" placeholder="12.87">
                        </div>
                        @if ($errors->has('valores_individuais'))
                        <span class="help-block">
                            <strong>{{ $errors->first('valores_individuais') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div id="dividir_conta_info"></div>

                

                <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
                    <label for="descricao">Descrição</label>
                    <input name="descricao" type="text" class="form-control" id="descricao" placeholder="Descrição da compra realizada" required>
                    @if ($errors->has('descricao'))
                    <span class="help-block">
                        <strong>{{ $errors->first('descricao') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('forma_pagamento') ? ' has-error' : '' }}">
                    <label for="forma_pagamento">Forma de pagamento</label>
                    <select name="forma_pagamento" class="form-control" id="forma_pagamento" required>
                        @foreach ($formas_pagamento as $forma_pagamento)
                        <option value="{{ $forma_pagamento->id }}">{{ $forma_pagamento->apelido }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('forma_pagamento'))
                    <span class="help-block">
                        <strong>{{ $errors->first('forma_pagamento') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                    <label for="tags">Tags</label>
                    <select multiple name="tags[]" class="form-control" id="tags" required style="height: 160px;">
                        @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}">{{ $tag->nome }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('tags'))
                    <span class="help-block">
                        <strong>{{ $errors->first('tags') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('users') ? ' has-error' : '' }}">
                    <label for="users">Com quem?</label>
                    <select multiple name="users[]" class="form-control" id="users" style="height: 160px;">
                        @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('users'))
                    <span class="help-block">
                        <strong>{{ $errors->first('users') }}</strong>
                    </span>
                    @endif
                </div>
            </form>

            <button type="submit" class="btn btn-primary" form="form1">Salvar</button>
            <a href="{{ url('/financeiro') }}" class="btn btn-danger" role="button">Cancelar</a>
            @isset($saida)

            <form id="form2" method="POST" action="{{ url('/financeiro/excluir') }}/{{ $saida->id }}">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger" form="form2">Excluir</button>
            </form>
            @endisset

        </div>
    </div>
</div>
@endsection