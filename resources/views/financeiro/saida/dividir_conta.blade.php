<div class="form-group{{ $errors->has('dividir_conta') ? ' has-error' : '' }}">
	<label for="dividir_conta">Dividir conta com a galera?</label>
	<input name="dividir_conta" type="checkbox" id="dividir_conta">
	@if ($errors->has('dividir_conta'))
	<span class="help-block">
		<strong>{{ $errors->first('dividir_conta') }}</strong>
	</span>
	@endif
</div>

<div id="input_dividir_conta_igualmente">

</div>

<div id="input_individual">

</div>

<div id="dividir_conta_info">

</div>