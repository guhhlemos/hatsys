@extends('layouts.application')

@section('content')

<script>
    $(function () {

        var saidas = null;

        @isset($saidas)
        saidas = @json($saidas);
        @endisset

        console.log(saidas);

        $(".price").inputmask('currency', {
            'prefix': 'R$ ',
            'rightAlign': false,
            'groupSeparator': '.',
            'radixPoint': ",",
        });

        var total = 0;
        var cartao_nu = 0;
        var cartao_bb = 0;
        var dinheiro = 0;

        $.each(saidas, function(key, saida){
            total += parseFloat(saida.valor);

            if (saida.forma_pagamento) {

                if (saida.forma_pagamento.apelido == "cartão nu") {
                    cartao_nu += parseFloat(saida.valor);
                }
                if (saida.forma_pagamento.apelido == "cartão bb") {
                    cartao_bb += parseFloat(saida.valor);
                }
                if (saida.forma_pagamento.apelido == "dinheiro") {
                    dinheiro += parseFloat(saida.valor);
                }
            }

        });

        total = total.toFixed(2);
        cartao_nu = cartao_nu.toFixed(2);
        cartao_bb = cartao_bb.toFixed(2);
        dinheiro = dinheiro.toFixed(2);

        $('#total').text(total);
        $('#cartao_nu').text(cartao_nu);
        $('#cartao_bb').text(cartao_bb);
        $('#dinheiro').text(dinheiro);

        @if (session('status'))
        $("#row_{{ session('id') }}").css("background-color", "yellow");
        @endif

        // Remove alerta após 10 segundos
        // $('#mensagem_status').delay(10000).fadeOut();

        // Formata a data
        // $('.date').inputmask('99/99/9999', {
        //     placeholder:"mm/dd/yyyy",
        // });

        var dados = [
        {
            name: 'Cartão Nu',
            y: parseFloat(cartao_nu)
        }, {
            name: 'Cartão BB',
            y: parseFloat(cartao_bb)
        }, {
            name: 'Dinheiro',
            y: parseFloat(dinheiro)
        }
        ];

        // var dados = [{
        //     name: 'Cartão Nu',
        //     y: parseFloat(cartao_nu),
        //     sliced: true,
        //     selected: true
        // }, {
        //     name: 'Internet Explorer',
        //     y: 11.84
        // }, {
        //     name: 'Firefox',
        //     y: 10.85
        // }, {
        //     name: 'Edge',
        //     y: 4.67
        // }, {
        //     name: 'Safari',
        //     y: 4.18
        // }, {
        //     name: 'Sogou Explorer',
        //     y: 1.64
        // }, {
        //     name: 'Opera',
        //     y: 1.6
        // }, {
        //     name: 'QQ',
        //     y: 1.2
        // }, {
        //     name: 'Other',
        //     y: 2.61
        // }];

        console.log(dados);

        // Highcharts.chart('grafico', {
        //     chart: {
        //         plotBackgroundColor: null,
        //         plotBorderWidth: null,
        //         plotShadow: false,
        //         type: 'pie'
        //     },
        //     title: {
        //         text: 'Gastos em Março, 2018'
        //     },
        //     tooltip: {
        //         pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        //     },
        //     plotOptions: {
        //         pie: {
        //             allowPointSelect: true,
        //             cursor: 'pointer',
        //             dataLabels: {
        //                 enabled: true,
        //                 format: '<b>{point.name}</b>: {point.percentage:.1f} %',
        //                 style: {
        //                     color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
        //                 }
        //             }
        //         }
        //     },
        //     series: [{
        //         name: 'Brands',
        //         colorByPoint: true,
        //         data: dados
        //         // data: [{
        //         //     name: 'Chrome',
        //         //     y: 61.41,
        //         //     sliced: true,
        //         //     selected: true
        //         // }, {
        //         //     name: 'Internet Explorer',
        //         //     y: 11.84
        //         // }, {
        //         //     name: 'Firefox',
        //         //     y: 10.85
        //         // }, {
        //         //     name: 'Edge',
        //         //     y: 4.67
        //         // }, {
        //         //     name: 'Safari',
        //         //     y: 4.18
        //         // }, {
        //         //     name: 'Sogou Explorer',
        //         //     y: 1.64
        //         // }, {
        //         //     name: 'Opera',
        //         //     y: 1.6
        //         // }, {
        //         //     name: 'QQ',
        //         //     y: 1.2
        //         // }, {
        //         //     name: 'Other',
        //         //     y: 2.61
        //         // }]
        //     }]
        // });

        $("#data").val( moment().format('YYYY-MM-DD') );

    });
</script>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            @if (session('status'))
            <div class="alert alert-success" id="mensagem_status">
                {{ session('id') }}: {{ session('status') }}
            </div>
            @endif

            <!-- <div id="grafico" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div> -->

            <h4>TOTAL: <p id="total"></p></h4>
            <h4>TOTAL CARTÃO NU: <p id="cartao_nu"></p></h4>
            <h4>TOTAL CARTÃO BB: <p id="cartao_bb"></p></h4>
            <h4>TOTAL DINHEIRO: <p id="dinheiro"></p></h4>

            <a href="{{ url('/financeiro/cadastrar') }}" class="btn btn-success" role="button" style="float: right;">Cadastrar</a>
            
            <div class="table-container">
                @include('financeiro.saida.table')
            </div>

        </div>
    </div>
</div>
@endsection