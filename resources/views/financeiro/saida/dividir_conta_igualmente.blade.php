<div class="form-group{{ $errors->has('dividir_conta_igualmente') ? ' has-error' : '' }}">
	<label for="dividir_conta_igualmente">Dividir conta igualmente?</label>
	<input name="dividir_conta_igualmente" type="checkbox" id="dividir_conta_igualmente">
	@if ($errors->has('dividir_conta_igualmente'))
	<span class="help-block">
		<strong>{{ $errors->first('dividir_conta_igualmente') }}</strong>
	</span>
	@endif
</div>