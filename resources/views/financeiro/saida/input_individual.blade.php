<div class="form-group{{ $errors->has('valores_individuais') ? ' has-error' : '' }}">
    <label for="valores_individuais">Valor {{ $user->name }}</label>
    <div class="input-group">
        <span class="input-group-addon">R$</span>
        <input font-family="monospace" type="text" class="form-control valores_individuais" name="valores_individuais[][{{ $user->id }}]" placeholder="12.87">
    </div>
    @if ($errors->has('valores_individuais'))
    <span class="help-block">
        <strong>{{ $errors->first('valores_individuais') }}</strong>
    </span>
    @endif
</div>