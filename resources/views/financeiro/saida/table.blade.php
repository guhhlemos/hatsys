<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Data</th>
            <th scope="col">Valor Total</th>
            <th scope="col">Valor Individual</th>
            <th scope="col">Descrição</th>
            <th scope="col">Forma de pagamento</th>
            <th scope="col">Tags</th>
            <th scope="col">Com quem?</th>
            <th scope="col">Conta dividida?</th>
            <th scope="col">Dividida igualmente?</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($saidas as $saida)
        <tr id="row_{{ $saida->id }}">
            <th scope="row">{{ $saida->id }}</th>
            <td class="date">{{ $saida->data }}</td>
            <td class="price">{{ $saida->valor }}</td>
            @if ( is_null($saida->valor_individual) )
            <td>-</td>
            @else
            <td class="price">{{ $saida->valor_individual }}</td>
            @endif
            <td>{{ $saida->descricao }}</td>
            <td><?php echo isset($saida->forma_pagamento) ? $saida->forma_pagamento->apelido : "-"?></td>
            <td>
                @foreach ($saida->tags as $tag)
                <p>{{ $tag->nome }}</p>
                @endforeach
            </td>
            <td>
                @foreach ($saida->childs as $child)
                <p>{{ $child->user->name }}</p>
                @endforeach
            </td>
            <td><?php echo ($saida->conta_dividida) ? "Sim" : "Não"?></td>
            <td><?php echo ($saida->conta_dividida_igualmente) ? "Sim" : "Não"?></td>
            <td><a href="{{ url('/financeiro/editar') }}/{{ $saida->id }}" class="btn btn-primary" role="button">Alterar</a></td>
        </tr>
        @endforeach
    </tbody>
</table>