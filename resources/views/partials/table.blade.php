<table class="table">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Data</th>
			<th scope="col">Valor</th>
			<th scope="col">Descrição</th>
			<th scope="col">Forma de pagamento</th>
			<th scope="col">Tags</th>
			<th scope="col"></th>
		</tr>
	</thead>
	<tbody>
		@isset($saidas)
		@foreach ($saidas as $saida)
		<tr id="row_{{ $saida->id }}">
			<th scope="row">{{ $saida->id }}</th>
			<td class="date">{{ $saida->data }}</td>
			<td class="price">{{ $saida->valor }}</td>
			<!-- <td class="valor">87233</td> -->
			<td>{{ $saida->descricao }}</td>
			<td>{{ $saida->forma_pagamento->apelido }}</td>
			<td>
				@foreach ($saida->tags as $tag)
				<p>{{ $tag->nome }}</p>
				@endforeach
			</td>
			<td><a href="{{ url('/financeiro/editar') }}/{{ $saida->id }}" class="btn btn-primary" role="button">Alterar</a></td>
		</tr>
		@endforeach
		@endisset
	</tbody>
</table>