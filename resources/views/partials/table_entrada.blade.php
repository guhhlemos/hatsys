<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Data</th>
            <th scope="col">Valor</th>
            <th scope="col">Descrição</th>
            <th scope="col">Tags</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($entradas as $entrada)
        <tr id="row_{{ $entrada->id }}">
            <th scope="row">{{ $entrada->id }}</th>
            <td class="date">{{ $entrada->data }}</td>
            <td class="price">{{ $entrada->valor }}</td>
            <td>{{ $entrada->descricao }}</td>
            <td>
                @foreach ($entrada->tags as $tag)
                <p>{{ $tag->nome }}</p>
                @endforeach
            </td>
            <td><a href="{{ url('/entrada/editar') }}/{{ $entrada->id }}" class="btn btn-primary" role="button">Alterar</a></td>
        </tr>
        @endforeach
    </tbody>
</table>