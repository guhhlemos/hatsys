<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Auth routes
 */
Route::group(['namespace' => 'Auth'], function () {

    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('logout', 'LoginController@logout')->name('logout');

    // Registration Routes...
    if (config('auth.users.registration')) {
        Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'RegisterController@register');
    }

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset');

    // Confirmation Routes...
    if (config('auth.users.confirm_email')) {
        Route::get('confirm/{user_by_code}', 'ConfirmController@confirm')->name('confirm');
        Route::get('confirm/resend/{user_by_email}', 'ConfirmController@sendEmail')->name('confirm.send');
    }

    // Social Authentication Routes...
    Route::get('social/redirect/{provider}', 'SocialLoginController@redirect')->name('social.redirect');
    Route::get('social/login/{provider}', 'SocialLoginController@login')->name('social.login');
});

/**
 * Backend routes
 */
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {

    // Dashboard
    Route::get('/', 'DashboardController@index')->name('dashboard');

    //Users
    Route::get('users', 'UserController@index')->name('users');
    Route::get('users/{user}', 'UserController@show')->name('users.show');
    Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit');
    Route::put('users/{user}', 'UserController@update')->name('users.update');
    Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy');
    Route::get('permissions', 'PermissionController@index')->name('permissions');
    Route::get('permissions/{user}/repeat', 'PermissionController@repeat')->name('permissions.repeat');
    Route::get('dashboard/log-chart', 'DashboardController@getLogChartData')->name('dashboard.log.chart');
    Route::get('dashboard/registration-chart', 'DashboardController@getRegistrationChartData')->name('dashboard.registration.chart');
});


Route::get('/', 'HomeController@index');

/**
 * Membership
 */
Route::group(['as' => 'protection.'], function () {
    Route::get('membership', 'MembershipController@index')->name('membership')->middleware('protection:' . config('protection.membership.product_module_number') . ',protection.membership.failed');
    Route::get('membership/access-denied', 'MembershipController@failed')->name('membership.failed');
    Route::get('membership/clear-cache/', 'MembershipController@clearValidationCache')->name('membership.clear_validation_cache');
});

Route::middleware('auth')->group(function () {

    // Dashboard
    Route::get('dashboard', 'DashboardController@index')->name('new_dashboard');

    Route::namespace('Financeiro')->group(function () {
    // Controllers Within The "App\Http\Controllers\Financeiro" Namespace

        Route::prefix('financeiro')->group(function () {

            Route::get('/', 'SaidaController@index')->name('financeiro_saida');
            Route::get('cadastrar', 'SaidaController@create');
            Route::post('cadastrar', 'SaidaController@store');
            Route::get('editar/{id}', 'SaidaController@edit');
            Route::post('editar/{id}', 'SaidaController@update');
            Route::post('excluir/{id}', 'SaidaController@destroy');
            Route::get('dividir_conta', 'SaidaController@dividir_conta');
            Route::get('dividir_conta_igualmente', 'SaidaController@dividir_conta_igualmente');
            Route::get('input_individual', 'SaidaController@input_individual');
        });

        Route::prefix('entrada')->group(function () {

            Route::get('/', 'EntradaController@index')->name('financeiro_entrada');
            Route::get('cadastrar', 'EntradaController@create');
            Route::post('cadastrar', 'EntradaController@store');
            Route::get('editar/{id}', 'EntradaController@edit');
            Route::post('editar/{id}', 'EntradaController@update');
            Route::post('excluir/{id}', 'EntradaController@destroy');
        });

        Route::prefix('formapagamento')->group(function () {

            Route::get('/', 'FormaPagamentoController@index');
            Route::get('cadastrar', 'FormaPagamentoController@create');
            Route::post('cadastrar', 'FormaPagamentoController@store');
            Route::get('editar/{id}', 'FormaPagamentoController@edit');
            Route::post('editar/{id}', 'FormaPagamentoController@update');
            Route::post('excluir/{id}', 'FormaPagamentoController@destroy');
        });

        Route::prefix('historico')->group(function () {

            Route::get('/', 'HistoricoController@index');
            Route::get('ajax', 'HistoricoController@ajax');
            // Route::get('cadastrar', 'HistoricoController@create');
            // Route::post('cadastrar', 'HistoricoController@store');
            // Route::get('editar/{id}', 'HistoricoController@edit');
            // Route::post('editar/{id}', 'HistoricoController@update');
            // Route::post('excluir/{id}', 'HistoricoController@destroy');
        });


    });
});